const readline = require('readline');

const interface = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

function question(q) {
    return new Promise(resolve => {
        interface.question(q, data => {
            resolve(data);
        });
    });
}

async function luasPermukaanLimas() {
    console.log(`
    ===VOLUME LIMAS SEGI TIGA===
    `);
    const a = await question('    Luas alas: ');
    const b = await question('    Tinggi limas: ');

    console.log(`    Volume Limas = ${1/3 * parseInt(a) * parseInt(b)}`);
}

luasPermukaanLimas();